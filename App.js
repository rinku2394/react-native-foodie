import React, { Component } from "react";
import { createStore } from "redux";
import { Provider } from "react-redux";

import Navigator from "./src/components/Navigator";

import reducers from "./src/reducers";

const store = createStore(reducers);

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Navigator />
      </Provider>
    );
  }
}

export default App;
