import { createStackNavigator, createAppContainer } from "react-navigation";

import Home from "./Home";
import Cart from "./Cart";
import Term from "./Terms";
import Login from './Login';
import SignUp from './SignUp'

const Navigator = createStackNavigator({
  Home: {
    screen: Home
  },
  Cart: {
    screen: Cart
  },
  Terms: {
    screen: Term
  },
  Login:{
    screen:Login
  },
  SignUp:{
    screen:SignUp
  }
});

export default createAppContainer(Navigator);
