import React, { Component } from "react";
import {
  View,
  Text,
  ScrollView,
  TouchableHighlight,
  StyleSheet,
  Image,
  StatusBar
} from "react-native";
import { connect } from "react-redux";

import { addToCartAction } from "../actions";

class Home extends Component {
  static navigationOptions = {
    header: null
  };

  renderItems = () => {
    const list = this.props.items.map(item => {
      return (
        <TouchableHighlight
          underlayColor="#ffcc66"
          activeOpacity={0.3}
          key={item.id}
          onPress={() => this.props.addToCartAction(item)}
        >
          <View>
            <Image
              style={styles.mainbox}
              source={{
                uri: item.imageUri
              }}
            />
            <Text>{item.name}</Text>
            <Text style={{ fontWeight: "bold" }}>Price : ${item.price}</Text>
          </View>
        </TouchableHighlight>
      );
    });
    return list;
  };

  render() {

    return (
      <View style={styles.contentContainer}>
        <StatusBar backgroundColor="brown" barStyle="dark-content" />

        <View style={styles.header}>
          <View style={styles.home}>
            <TouchableHighlight
              onPress={() => this.props.navigation.navigate("Login")}
            >
              <Text style={{ fontSize: 20, color: "brown" }}>Login</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.heading}>
            <Text style={{ color: "brown", fontSize: 24, fontWeight: "bold" }}>
              Foodie
            </Text>
          </View>
          <View style={styles.cart}>
            <TouchableHighlight
              underlayColor="#ffcc66"
              activeOpacity={0.3}
              onPress={e => this.props.navigation.navigate("Cart")}
            >
              <View>
                <Text
                  style={{
                    fontWeight: "bold",
                    fontSize: 18,
                    color: "orange",
                    marginLeft: 23,
                    marginTop: -7,
                    position: "absolute"
                  }}
                >
                  {this.props.cart.length}
                </Text>
                <Image
                  source={{
                    uri:
                      "https://www.drones4.life/wp-content/uploads/2016/12/shopping-cart-logo-icon-70706.png"
                  }}
                  style={{ width: 50, height: 50 }}
                />
              </View>
            </TouchableHighlight>
          </View>
        </View>

        <View style={styles.body}>
          <ScrollView style={{ padding: 15 }}>
            <Text style={{ fontSize: 20, color: "brown", marginBottom: 10 }}>
              Our Specials
            </Text>
            <View style={styles.headerScrollbar}>
              <TouchableHighlight onPress={() => alert("Chinese Manchurian")}>
                <View
                  style={{
                    flex: 1,
                    flexWrap: "wrap",
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                >
                  <Image
                    source={{
                      uri:
                        "https://www.archanaskitchen.com/images/archanaskitchen/1-Author/Dhara_Shah_/Chinese_Dry_Manchurian_balls_No_onion_No_garlic.jpg"
                    }}
                    style={styles.box}
                  />
                  <Text>Chinese Manchurian</Text>
                </View>
              </TouchableHighlight>
              <TouchableHighlight onPress={() => alert("Chilli Paneer")}>
                <View
                  style={{
                    flex: 1,
                    flexWrap: "wrap",
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                >
                  <Image
                    source={{
                      uri:
                        "http://www.sanjeevkapoor.com/UploadFiles/RecipeImages/Chilli-Paneer-Best-of-Chinese-Cooking.jpg"
                    }}
                    style={styles.box}
                  />
                  <Text>Chilli Paneer</Text>
                </View>
              </TouchableHighlight>
              <TouchableHighlight onPress={() => alert("Hakka Noodles")}>
                <View
                  style={{
                    flex: 1,
                    flexWrap: "wrap",
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                >
                  <Image
                    source={{
                      uri:
                        "https://rasamalaysia.com/wp-content/uploads/2018/07/brown-butter-garlic-noodles1.jpg"
                    }}
                    style={styles.box}
                  />
                  <Text>Hakka Noodles</Text>
                </View>
              </TouchableHighlight>
            </View>
            <Text style={{ fontSize: 20, color: "brown", marginTop: 20 }}>
              Appetizers
            </Text>
            <View style={styles.bodyScrollbar}>{this.renderItems()}</View>
            <View style={styles.footerScrollbar} />
          </ScrollView>
        </View>

        <View style={styles.footer}>
          <TouchableHighlight
            underlayColor="#ffcc66"
            activeOpacity={0.3}
            onPress={() => this.props.navigation.navigate("Terms")}
          >
            <Text style={{ color: "brown" }}>
              Terms &amp; Conditions &copy; | 2019
            </Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    items: state.itemReducer,
    cart: state.cartReducer
  };
};

export default connect(
  mapStateToProps,
  { addToCartAction }
)(Home);

const styles = StyleSheet.create({
  contentContainer: {
    flex: 1
  },
  header: {
    flex: 1,
    flexDirection: "row"
  },
  home: {
    flexGrow: 2,
    justifyContent: "center",
    alignItems: "center"
  },
  heading: {
    flexGrow: 6,
    justifyContent: "center",
    alignItems: "center"
  },
  cart: {
    flexGrow: 2,
    justifyContent: "center",
    alignItems: "center"
  },
  body: {
    flex: 8
  },
  headerScrollbar: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center"
  },
  box: {
    height: 100,
    width: 100
  },
  bodyScrollbar: {
    flex: 3,
    flexDirection: "row",
    justifyContent: "space-around",
    flexWrap: "wrap",
    marginTop: 30
  },
  mainbox: {
    height: 150,
    width: 150,
    marginTop: 20
  },
  footerScrollbar: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center"
  },
  footer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});
