import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  Button,
  StatusBar
} from "react-native";
import { connect } from "react-redux";

import { removeFromCartAction } from "../actions";
import { TouchableOpacity } from "react-native-gesture-handler";

class Cart extends Component {
  static navigationOptions = {
    title: "Cart"
  };

  renderTotal = () => {
    let total = 0;
    this.props.store.map(item => {
      total = total + item.price;
    });

    return total;
  };

  renderItems = () => {
    const cartItems = this.props.store;
    if (cartItems.length !== 0) {
      const list = cartItems.map((item, index) => {
        return (
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              marginHorizontal: "3%"
            }}
            key={item.id}
          >
            <View style={{ width: "20%" }}>
              <Image
                source={{ uri: item.imageUri }}
                style={{ width: 50, height: 50 }}
              />
            </View>
            <View style={{width: "50%" }}>
            <Text style={{ fontSize: 15}}>{item.name}</Text>
            <Text>- 1 +</Text>
            </View>
            <View style={{ width: "20%" }}>
              <Button
                onPress={() => this.props.removeFromCartAction(index)}
                title="Remove"
              />
            </View>
          </View>
        );
      });
      return list;
    } else {
      return (
        <View>
          <Text>Your Cart Is Empty!</Text>
        </View>
      );
    }
  };

  render() {

    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="brown" barStyle="dark-content" />
        <View style={styles.header}>
          <Text style={{ color: "white", fontSize: 22, color: "brown" }}>
            Items In Your Cart
          </Text>
        </View>
        <View style={styles.body}>
          <ScrollView>{this.renderItems()}</ScrollView>
        </View>
        <View style={styles.footer}>
          <View style={{ flex: 1,marginHorizontal:'5%' }}>
            <Text style={{fontSize:20}}>Cart Total:</Text>
          </View>
          <View
            style={{
              flex: 1,
              marginHorizontal:'5%'
            }}
          >
            <Text style={{fontSize:24}}>${this.renderTotal()}</Text>
            <TouchableOpacity>
              <Text>Chekout</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    store: state.cartReducer
  };
};

export default connect(
  mapStateToProps,
  { removeFromCartAction }
)(Cart);

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  header: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  body: {
    flex: 5,
    backgroundColor: "white"
  },
  footer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderTopWidth:2,
    borderTopColor:'orange'
  }
});
