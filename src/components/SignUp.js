import React, { Component } from "react";
import { View, Text, StyleSheet, StatusBar, TextInput } from "react-native";
import {
  TouchableHighlight,
  TouchableOpacity
} from "react-native-gesture-handler";

export default class SignUp extends Component {
  state = {
    name: "",
    email: "",
    password: "",
    cpassword: "",
    nameerror: false,
    emailerror: false,
    passworderror: false,
    cpassworderror: false
  };

  static navigationOptions = {
    header: null
  };

  handleName = () => {
    if (this.state.name === "") {
      this.setState({
        nameerror: true
      });
    } else {
      this.setState({
        nameerror: false
      });
    }
  };

  handleEmail = () => {
    if (this.state.name === "") {
      this.setState({
        emailerror: true
      });
    } else {
      this.setState({
        emailerror: false
      });
    }
  };

  handlePassword = () => {
    if (this.state.name === "") {
      this.setState({
        passworderror: true
      });
    } else {
      this.setState({
        passworderror: false
      });
    }
  };

  handleCPassword = () => {
    if (this.state.name === "") {
      this.setState({
        cpassworderror: true
      });
    } else {
      this.setState({
        cpassworderror: false
      });
    }
  };

  handleClickChange = () => {
    this.handleName();
    this.handleEmail();
    this.handlePassword();
    this.handleCPassword();
  };

  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="brown" barStyle="dark-content" />
        <View style={styles.header}>
          <TouchableHighlight
            underlayColor="#ffcc66"
            activeOpacity={0.3}
            onPress={() => this.props.navigation.popToTop()}
          >
            <Text style={{ fontSize: 30, color: "brown", fontWeight: "bold" }}>
              Foodie
            </Text>
          </TouchableHighlight>
        </View>
        <View style={styles.body}>
          <View style={styles.formheader}>
            <Text style={{ fontSize: 24, color: "black" }}>Sign Up</Text>
          </View>
          <View style={styles.formbody}>
            <View style={{ marginBottom: 10 }}>
              <Text style={{ fontSize: 18, color: "brown" }}>Name</Text>
              <TextInput
                style={{
                  borderBottomColor: "black",
                  borderBottomWidth: 2,
                  width: 300
                }}
                onChangeText={e => this.setState({ name: e })}
              />
              <View>
                <Text style={styles.error}>{this.state.nameerror && 'Please Enter A Name'}</Text>
              </View>
            </View>
            <View style={{ marginBottom: 10 }}>
              <Text style={{ fontSize: 18, color: "brown" }}>Email</Text>
              <TextInput
                style={{
                  borderBottomColor: "black",
                  borderBottomWidth: 2,
                  width: 300
                }}
                onChangeText={e => this.setState({ email: e })}
              />
              <View>
                <Text style={styles.error}>{this.state.nameerror && 'Please Enter An Email'}</Text>
              </View>
            </View>
            <View style={{ marginBottom: 10 }}>
              <Text style={{ fontSize: 18, color: "brown" }}>Password</Text>
              <TextInput
                style={{
                  borderBottomColor: "black",
                  borderBottomWidth: 2,
                  width: 300
                }}
                onChangeText={e => this.setState({ password: e })}
              />
              <View>
                <Text style={styles.error}>{this.state.nameerror && 'Please Enter A Password'}</Text>
              </View>
            </View>
            <View style={{ marginBottom: 10 }}>
              <Text style={{ fontSize: 18, color: "brown" }}>
                Confirm Password
              </Text>
              <TextInput
                style={{
                  borderBottomColor: "black",
                  borderBottomWidth: 2,
                  width: 300
                }}
                onChangeText={e => this.setState({ cpassword: e })}
              />
              <View style={{marginBottom:10}}>
                <Text style={styles.error}>{this.state.nameerror && 'Password Provided Is Not Same'}</Text>
              </View>
            </View>
          </View>
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <TouchableOpacity
              onPress={() => alert("Go To SignUp")}
              style={{
                borderWidth: 1,
                width: 100,
                height: 50,
                justifyContent: "center",
                alignItems: "center",
                borderRadius: 5,
                borderColor: "brown"
              }}

              onPress={this.handleClickChange}
            >
              <Text style={{ fontSize: 18, color: "brown" }}>Sign Up</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.footer}>
          <TouchableOpacity
            activeOpacity={0.3}
            underlayColor="#ffcc66"
            onPress={() => this.props.navigation.navigate("Terms")}
          >
            <Text style={{ fontSize: 18, color: "brown" }}>
              Terms &amp; Conditions &copy; | 2019
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  header: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  body: {
    flex: 6
  },
  formheader: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  formbody: {
    flex: 4,
    justifyContent: "center",
    alignItems: "center"
  },
  footer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  error:{
    color:"red"
  }
});
