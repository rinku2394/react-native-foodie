import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableHighlight,
  Image,
  TextInput,
  Button,
  StatusBar
} from "react-native";

import { connect } from "react-redux";

class Login extends Component {
  state = { email: "", password: "", emailerror: false, passworderror: "" };

  static navigationOptions = {
    header: null
  };

  handleEmail = () => {
    if (this.state.email === "") {
      this.setState(prevstate => {
        return {
          emailerror: true
        };
      });
    } else {
      this.setState(prevstate => {
        return {
          emailerror: false
        };
      });
    }
  };

  handlePassword = () => {
    if (this.state.email === "") {
      this.setState(prevstate => {
        return {
          passworderror: true
        };
      });
    } else {
      this.setState(prevstate => {
        return {
          passworderror: false
        };
      });
    }
  };

  handleLoginClick = () => {
    this.handleEmail()
    this.handlePassword()
  };

  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="brown" barStyle="dark-content" />

        <View style={styles.header}>
          <View style={styles.signup}>
            <TouchableHighlight
              underlayColor="#ffcc66"
              activeOpacity={0.3}
              onPress={() => this.props.navigation.navigate("SignUp")}
            >
              <Text style={{ fontSize: 20, color: "brown", marginLeft: "5%" }}>
                Sign Up
              </Text>
            </TouchableHighlight>
          </View>
          <View style={styles.heading}>
            <Text style={{ fontSize: 24, color: "brown", fontWeight: "bold" }}>
              Foodie
            </Text>
          </View>
          <View style={styles.cart}>
            <TouchableHighlight
              underlayColor="#ffcc66"
              activeOpacity={0.3}
              onPress={e => this.props.navigation.navigate("Cart")}
            >
              <View>
                <Text
                  style={{
                    fontWeight: "bold",
                    fontSize: 18,
                    color: "orange",
                    marginLeft: 23,
                    marginTop: -7,
                    position: "absolute"
                  }}
                >
                  {this.props.store.length}
                </Text>
                <Image
                  source={{
                    uri:
                      "https://www.drones4.life/wp-content/uploads/2016/12/shopping-cart-logo-icon-70706.png"
                  }}
                  style={{ width: 50, height: 50 }}
                />
              </View>
            </TouchableHighlight>
          </View>
        </View>
        <View style={styles.body}>
          <View style={styles.formheader}>
            <Text style={{ color: "black", fontSize: 26 }}>Login here</Text>
          </View>
          <View style={styles.formbody}>
            <View style={styles.formcontrol}>
              <Text style={{ fontSize: 20, color: "brown" }}>Username</Text>
              <TextInput
                style={{
                  borderBottomColor: "black",
                  borderBottomWidth: 2,
                  width: 300
                }}

                onChangeText={e=>this.setState({email:e})}
              />
              <View>
              <Text style={styles.error}>{this.state.emailerror && (
                  'Please Enter Username'
                )}</Text>
              </View>
            </View>
            <View style={styles.formcontrol}>
              <Text style={{ fontSize: 20, color: "brown" }}>Password</Text>
              <TextInput
                style={{
                  borderBottomColor: "black",
                  borderBottomWidth: 2,
                  width: 300
                }}
                onChangeText={e=>this.setState({password:e})}
              />
              <View>
                <Text style={styles.error}>{this.state.passworderror && (
                  'Please Enter Password'
                )}</Text>
              </View>
              <Text />
            </View>
            <View>
              <TouchableHighlight
                underlayColor="#ffcc66"
                activeOpacity={0.3}
                style={{
                  marginTop: 20,
                  borderWidth: 1,
                  borderColor: "brown",
                  width: 100,
                  height: 50,
                  borderRadius: 5,
                  justifyContent: "center",
                  alignItems: "center"
                }}
                onPress={this.handleLoginClick}
              >
                <Text style={{ fontSize: 18, color: "brown" }}>Login</Text>
              </TouchableHighlight>
            </View>
          </View>
          <View style={styles.formfooter}>
            <Text style={{ fontSize: 20, color: "orange" }}>
              Don't Have An Account? Sign Up First!
            </Text>
          </View>
        </View>

        <View style={styles.footer}>
          <TouchableHighlight
            underlayColor="#ffcc66"
            activeOpacity={0.3}
            onPress={() => this.props.navigation.navigate("Terms")}
          >
            <Text style={{ fontSize: 18, color: "brown" }}>
              Terms &amp; Conditions &copy; | 2019
            </Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    store: state.cartReducer
  };
};

export default connect(mapStateToProps)(Login);

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  header: {
    flex: 1,
    flexDirection: "row"
  },
  signup: {
    flex: 1,
    flexGrow: 2,
    justifyContent: "center",
    alignItems: "center"
  },
  heading: {
    flex: 1,
    flexGrow: 6,
    justifyContent: "center",
    alignItems: "center"
  },
  cart: {
    flex: 1,
    flexGrow: 2,
    justifyContent: "center",
    alignItems: "center"
  },
  body: {
    flex: 7
  },
  formheader: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  formbody: {
    flex: 4,
    justifyContent: "center",
    alignItems: "center"
  },
  formcontrol: {
    marginTop: 20
  },
  formfooter: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  footer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  error: {
    color: "red"
  }
});
