import React, { Component } from "react";
import { View, Text, StyleSheet, ScrollView, StatusBar } from "react-native";

export default class Terms extends Component {
  static navigationOptions = {
    header: null
  };

  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="brown" barStyle="dark-content" />
        <View style={styles.header}>
          <Text style={{ color: "black", fontSize: 24 }}>
            Terms &amp; Conditions
          </Text>
        </View>
        <View style={styles.body}>
          <ScrollView>
            <Text style={{ fontSize: 20, color: "brown" }}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua.
              Tristique magna sit amet purus gravida. Mauris cursus mattis
              molestie a iaculis. Cursus mattis molestie a iaculis at erat
              pellentesque adipiscing commodo. Feugiat nisl pretium fusce id
              velit ut tortor. Sed ullamcorper morbi tincidunt ornare massa eget
              egestas purus viverra.
            </Text>
            <Text style={{ fontSize: 20, color: "brown", marginTop: 10 }}>
              Ac tincidunt vitae semper quis lectus nulla at volutpat. Diam in
              arcu cursus euismod quis. Libero enim sed faucibus turpis in eu
              mi. Lacinia at quis risus sed. Aliquet lectus proin nibh nisl
              condimentum id venenatis. Dictum sit amet justo donec enim diam
              vulputate ut. Tristique magna sit amet purus gravida quis blandit
              turpis cursus. Nullam vehicula ipsum a arcu cursus vitae congue
              mauris. Est sit amet facilisis magna etiam tempor. Ligula
              ullamcorper malesuada proin libero nunc consequat interdum.
            </Text>
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  header: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  body: {
    flex: 7,
    padding: 20
  }
});
