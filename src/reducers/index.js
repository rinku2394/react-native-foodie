import { combineReducers } from "redux";

const initialState = [
  {
    id: 1,
    name: "Hakka Noodles",
    price:110.16,
    imageUri:
      "https://rasamalaysia.com/wp-content/uploads/2018/07/brown-butter-garlic-noodles1.jpg"
  },
  {
    id: 2,
    name: "Chinese Gravy Munchurian",
    price:10.16,
    imageUri:
      "https://www.archanaskitchen.com/images/archanaskitchen/1-Author/Dhara_Shah_/Chinese_Dry_Manchurian_balls_No_onion_No_garlic.jpg"
  },
  {
    id: 3,
    name: "Chinese Dry Manchurian",
    price:20.16,
    imageUri:
      "https://www.archanaskitchen.com/images/archanaskitchen/1-Author/Dhara_Shah_/Chinese_Dry_Manchurian_balls_No_onion_No_garlic.jpg"
  },
  {
    id: 4,
    name: "ChilliPaneer",
    price:40.16,
    imageUri:
      "http://www.sanjeevkapoor.com/UploadFiles/RecipeImages/Chilli-Paneer-Best-of-Chinese-Cooking.jpg"
  },
  {
    id: 5,
    name: "Hakka Noodles",
    price:35.16,
    imageUri:
      "https://rasamalaysia.com/wp-content/uploads/2018/07/brown-butter-garlic-noodles1.jpg"
  },
  {
    id: 6,
    name: "Chinese Gravy Munchurian",
    price:18.16,
    imageUri:
      "https://www.archanaskitchen.com/images/archanaskitchen/1-Author/Dhara_Shah_/Chinese_Dry_Manchurian_balls_No_onion_No_garlic.jpg"
  },
  {
    id: 7,
    name: "Chinese Dry Manchurian",
    price:28.16,
    imageUri:
      "https://www.archanaskitchen.com/images/archanaskitchen/1-Author/Dhara_Shah_/Chinese_Dry_Manchurian_balls_No_onion_No_garlic.jpg"
  }
];

const userState = {
    isLoggedIn:'false'
}

const cartReducer = (state = [], action) => {
  if (action.type === "ADD_TO_CART") {
    return [...state, action.payload];
  }else if(action.type === "REMOVE_FROM_CART"){
      const newState = state.filter((item,index)=> index !== action.payload)
      return newState
  }
  return state;
};

const itemReducer = (state=initialState) =>{
    return state
}

const userReducer = (state=userState,action)=>{
  return state
}


const reducers = combineReducers({
  cartReducer,
  itemReducer,
  userReducer
});

export default reducers;
