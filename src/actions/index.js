export const addToCartAction = (item)=>{
    return{
        type:"ADD_TO_CART",
        payload:item
    }
}

export const removeFromCartAction = (index)=>{
    return{
        type:"REMOVE_FROM_CART",
        payload:index
    }
}